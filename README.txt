
QueryPath filter is a module that provides a customisable input filter.
The output may be atered by using CSS3 jQuery style selectors.
This module requires the QueryPath module, which includes the QueryPath library


Installation
-------------------------------------------------------------------------------

Copy the folder to your module directory and then enable on the admin
modules page. You must install the QueryPath module too.


Usage
-------------------------------------------------------------------------------

Once installed, go to admin/config/content/querypath_filter to manage rules.
Then activate the QueryPath filter to the text formats you want to be altered
by the filter.

Give the "Administer rules" permission to the users that should be able to
manage the rules.


Author
-------------------------------------------------------------------------------
dolu
http://drupal.org/user/374670
